# Partie 1
## Sous-partie 1 : texte

Une phrase sans rien

_Une phrase en italique_

**Une phrase en gras**

Un lien vers [fun-mooc.fr](www.fun-mooc.fr)

Une ligne de code
## Sous-partie 2 : listes
**Liste à puce**
* item
  * sous-item
  * sous-item
* item
* item

**Liste à numéroté**
1. Item1
2. Item2
3. Item3
## Sous-partie 3 : code

```javascript
# Extrait de code